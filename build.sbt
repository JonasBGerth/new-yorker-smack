name := "New_Yorker_SMACK"

version := "1.0.1"

scalaVersion := "2.13.1"

val circeVersion = "0.12.1"
val alpakkaVersion = "1.1.2"

libraryDependencies += "com.typesafe.akka" %% "akka-stream" % "2.6.0"
libraryDependencies += "com.lightbend.akka" %% "akka-stream-alpakka-cassandra" % alpakkaVersion

libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.2.3"

libraryDependencies ++= Seq(
  "io.circe" %% "circe-core",
  "io.circe" %% "circe-generic",
  "io.circe" %% "circe-parser", 
  "io.circe" %% "circe-generic-extras"
).map(_ % circeVersion)

assembly / mainClass := Some("de.joge.smack.DataImport")

assemblyMergeStrategy in assembly := {
  case PathList("reference.conf") => MergeStrategy.concat
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first
}
