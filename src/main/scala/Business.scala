package de.joge.smack

case class Business (
                    business_id: String,
                    name: String,
                    address: String,
                    city: String,
                    state: String,
                    postal_code: String,
                    latitude: Double,
                    longitude: Double,
                    stars: Float,
                    review_count: Int,
                    is_open: Int,
                    attributes: Option[Attributes] = None,
                    categories: Option[String]= None,
                    hours: Option[Hours] = None
                    )

case class Attributes(
                     RestaurantTakeOut: Option[String] = None,
                     WheelchairAccessible: Option[String] = None,
                     Smoking: Option[String] = None,
                     Caters: Option[String] = None,
                     RestaurantsReservations: Option[String] = None,
                     GoodForDancing: Option[String] = None,
                     OutdoorSeating: Option[String] = None,
                     Alcohol: Option[String] = None,
                     HasTV: Option[String] = None,
                     CoatCheck: Option[String] = None,
                     DogsAllowed: Option[String] = None,
                     Wifi: Option[String] = None,
                     HappyHour: Option[String] = None,
                     BikeParking: Option[String] = None,
                     RestaurantsTableService: Option[String] = None,
                     GoodForMeal: Option[String] = None,
                     RestaurantsGoodForGroups: Option[String] = None,
                     RestaurantsPriceRange2: Option[String] = None,
                     BusinessParking: Option[String] = None,
                     ByAppointmentOnly: Option[String] = None,
                     RestaurantsAttire: Option[String] = None,
                     Ambiance: Option[String] = None,
                     GoodForKids: Option[String] = None,
                     NoiseLevel: Option[String] = None,
                     RestaurantsDelivery: Option[String] = None,
                     BusinessAcceptsCreditCards: Option[String] = None
                     )

case class Hours(
                Monday: Option[String] = None,
                Tuesday: Option[String] = None,
                Wednesday: Option[String] = None,
                Thursday: Option[String] = None,
                Friday: Option[String] = None,
                Saturday: Option[String] = None,
                Sunday: Option[String] = None
                )