package de.joge.smack

case class Photo (
                 caption: String,
                 photo_id: String,
                 business_id: String,
                 label: String
                 )
