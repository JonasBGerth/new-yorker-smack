package de.joge.smack

case class Review (
                  review_id: String,
                  user_id: String,
                  business_id: String,
                  stars: Float,
                  useful: Int,
                  funny: Int,
                  cool: Int,
                  text: String,
                  date: String
                  )