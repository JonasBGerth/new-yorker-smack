FROM openjdk:8

WORKDIR /opt/smack

ARG PATH_TO_DATA_SET=./
ARG DATA_SET_NAME=yelp_dataset.tar

COPY $PATH_TO_DATASET$DATA_SET_NAME /opt/smack

RUN tar -xvzf yelp_dataset.tar

ARG A_JAR_NAME=New_Yorker_SMACK-assembly-1.0.1.jar

COPY ./target/scala-2.13/$A_JAR_NAME /opt/smack/

ENV CASSANDRA_ADDRESS '127.0.0.1'
ENV CASSANDRA_PORT '9042'
ENV JAR_NAME $A_JAR_NAME

CMD java -jar $JAR_NAME --cassandraAddress $CASSANDRA_ADDRESS --cassandraPort $CASSANDRA_PORT
